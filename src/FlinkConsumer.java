import edu.purdue.cs.aggr.FlinkAccumulator;
import edu.purdue.cs.aggr.Sender;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.zip.GZIPInputStream;

/**
 * Created by ercoppa on 14/05/15.
 */
public class FlinkConsumer {

		private static ArrayList<FlinkAccumulator> list = new ArrayList<FlinkAccumulator>();

		private static void getAccumulator(ObjectInputStream input) {

			try {

				Object obj = input.readObject();
				if (obj instanceof java.net.SocketException) {
					try {
						throw (java.net.SocketException) obj;
					} catch (SocketException e) {
						e.printStackTrace();
						System.exit(1);
					}
				}

				FlinkAccumulator acc = (FlinkAccumulator) obj;
				//System.out.println(acc.getAccumulation());
				synchronized (list) {
					list.add(acc);
					if (list.size() == 2) {
						mergeAccumulators();
						list.clear();
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.exit(1);
			}

		}

		public static void mergeAccumulators() {

			// aggregate
			list.get(0).addAccumulators(list.subList(1, list.size()));

			// print
			System.out.println("RESULT AGGREGATION: " + list.get(0).getAccumulation());

			// send back to Flink
			sendBackAccumulator(list.get(0));
		}

		public static void sendBackAccumulator(FlinkAccumulator acc) {

			Sender.Connection c = Sender.initConnection("127.0.0.1", 6000);
			try {
				c.output.writeObject(acc);
			} catch (IOException e) {
				e.printStackTrace();
			}
			Sender.closeConnection(c);
		}

		public static boolean handleMessage(ObjectInputStream input, String sender) {

			try {

				//System.err.println("Waiting for message from sender: " + sender);

				Object obj = input.readObject();
				if (obj instanceof java.net.SocketException) {
					try {
						throw (java.net.SocketException) obj;
					} catch (SocketException e) {
						e.printStackTrace();
						System.exit(1);
					}
				}

				Sender.MESSAGE kind = (Sender.MESSAGE) obj;
				System.out.println("Received message: " + kind.name()
															+ " from " + sender);

				switch (kind) {

				case HELLO:
					break;

				case ACCUMULATOR:
					getAccumulator(input);
					break;

				default:
					System.err.println("Invalid message kind: " + kind);
				}

			} catch (EOFException e) {
				System.err.println("EOF from " + sender);
				return true;
			} catch (IOException e) {
				e.printStackTrace();
				return true;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return true;
			}

			return false;
		}

		public static class Receiver implements Runnable {

			Socket socket = null;
			ObjectInputStream input = null;

			public Receiver(Socket socket) throws IOException {

				//System.out.println("Creating receiver " + socket.toString());

				this.socket = socket;

				//System.out.println("Creating receiver [2] " + socket.toString());

				this.input = new ObjectInputStream(new GZIPInputStream(socket.getInputStream()));

				//System.out.println("Created receiver " + socket.toString());
			}

			@Override
			public void run() {
				try {
					//System.out.println("Started receiver for " + socket.toString());

					boolean stop = false;
					while (!stop) {
						stop = handleMessage(input, socket.toString());
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.exit(1);
				}
			}

		}

    public static void main(String[] args) {

      ServerSocket serverSocket = null;

      try {

				serverSocket = new ServerSocket(5000);
				System.out.println("Server is listening...");

				while (true) {

					Socket socket = serverSocket.accept();
					//System.out.println("New incoming connection from: " + socket.toString());

					Receiver r = new Receiver(socket);

					//System.out.println("Receiver created OK: " + socket.toString());

					Thread t = new Thread(r);
					t.start();

					//System.out.println("Started thread: " + socket.toString());
				}

      } catch (IOException e) {
        e.printStackTrace();
        System.exit(1);
      }

			// good bye
			try {
				serverSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(1);
			}
    }

}

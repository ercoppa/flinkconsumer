import org.apache.flink.runtime.noah.Sender;

import java.io.IOException;

/**
 * Created by ercoppa on 17/05/15.
 */
public class FlinkProducer {

	public static void main(String[] args) {

		Sender.Connection c = Sender.initConnection("127.0.0.1", 5000);
		try {
			c.output.writeObject(Sender.MESSAGE.HELLO);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Sender.closeConnection(c);

	}

}

